#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_gta9.mk

COMMON_LUNCH_CHOICES := \
    lineage_gta9-user \
    lineage_gta9-userdebug \
    lineage_gta9-eng
