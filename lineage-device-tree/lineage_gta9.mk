#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from gta9 device
$(call inherit-product, device/samsung/gta9/device.mk)

PRODUCT_DEVICE := gta9
PRODUCT_NAME := lineage_gta9
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-X115
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="gta9xx-user 14 UP1A.231005.007 X115XXS3BXG4 release-keys"

BUILD_FINGERPRINT := samsung/gta9xx/gta9:14/UP1A.231005.007/X115XXS3BXG4:user/release-keys
